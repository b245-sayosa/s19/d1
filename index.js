console.log("Hello world")

// What is conditional statements
	// Conditional statemenst allow us to control the flow of our program
	// It allows us to run the statement/instruction if a condition is met or run another separate instruction if not otherwise

// [SECTION] IF ELSE

let numA = -1;

/*
	if Statement
		-it will execute the statement/code blocks if a specified condition is met/true


*/

if(numA<0){
	console.log("Hello from numA.")
}

console.log(numA<0)

/*
Syntax:
	if(condition){
		Statemen;
	}

*/

// The result of the expression added in the if's condition must result to true, else the statement inside the if() will not run.

// lets reassign the variable numA and run an if statement with the same condition

numA = 1

if(numA<0){
	console.log("Hello from the reassigned value of numA")
}
console.log(numA<0)

// It will not run because the expression now results to false.

let city = "New York";

if(city === "New York"){

	console.log("Welcome to New York!")
}

// IF ELSE CLAUSE

/*
 -Executes a statement if previous conditions are false and if the specified condition is true

 -the "else if" is optional and can be added to capture additional conditions to change the flow of a program

*/

let numH = 1;

if(numH<0){
	console.log("Hello from (numH<0).");
}
else if(numH>0){
	console.log("Hello from (numH>0).");
}


/*
We were able to run the else if() statement after we evaluated that the if condition was failed /false

// if the if condition was passed and run we will no longer evaluate to else if() and end the process



*/




{
  	//this is a code block

  	 // else if is dependent with if ,tou cannot use else if clause alone. 
}


if(numH !== 1){
	console.log("hello from numH ===1!")
}
else if(numH === 1){
	console.log("hello from numH>0!")
}
else if(numH>0){
	console.log("Hello from the second else if clause")
}


city = "Tokyo";


if(city === "New York"){

	console.log("Welcome to New York!")
}
else if(city === "Tokyo"){
	console.log("Welcome to Tokyo!");
}

// else statement

	/*
		Execute a statement if all other conditions are false not met-

		-else statement is optional and canbe added to capture any other result to change the flow of program.

	*/

numH = 2;

if(numH<0){
	console.log("hello from if statement");
}
else if(numH>2){
	console.log("hello from the firts else if");
}
else if(numH>3){
	console.log("hello from the second else if");
}

// if all conditions above is not met else statement will run
else{
	console.log("hello from the else statement.");
}

// since all of the preceeding if and else conditions were not met , the else statement was executed instead
// else statement is also dependent with if statement, cannot go alone.

{
	//dependent on if
	// else{
	// 	console.log("Hello")
	// }
}

let message;

function determineTyphoonIntensity(windSpeed){
	if(windSpeed<0){
		return "Invalid argument!";
	}
	else if(windSpeed<=30){
		return "Not a typhoon yet!"
	}
	else if(windSpeed<=60){
		return "Tropical Depression"
	}
	else if(windSpeed<=88){
		return "Tropical Storm detected"
	}
	else if(windSpeed<=117){
		return "Severe Tropical Storm detected"
	}
	else if(windSpeed>117){
		return "Typhoon Detected"
	}
	else if(windSpeed>=118)
		return "Typhoon detected."
	else{
		return "invalid input";
	}
}



	//mini-activity
	//1. add return tropical storm detected if the windspeed is between 60 and 89
	//2. add return "Severe Tropical Storm Detected" if the windspeed is between 88 and 118
	//3. if higher than 117 return "Typhoon detected!"


console.log(determineTyphoonIntensity("q"))

message = determineTyphoonIntensity(119);
if(message === "Typhoon Detected"){		
// console.warn() is a good way to print warning in our console that could help us developers act on certaion output within our code
		console.warn(message);
	}


// [SECTION] Truthy or falsy
	// Javascript a "tuthy" is a value that is considered true when encountered in boolean context
	// values are considered true unless defined otherwise
	// falsy values/ exeption for truthy
	/*
		1.false
		2.0
		3.-0
		4.""
		5.null
		6.undefine
		7.NaN
	*/

if(true){
	console.log("true");
}
if(1){
	console.log("true");
}
if(null){
	console.log("Hello from null inside the if condition.")
}
else{
	console.log("hello from the else of the null")
}

// [SECTION] Condition operator/ Ternary operator
/*
	The Conditional Operator
	1. condition
	2.expression to execute if the condition is true or truthy
	3.expression if the condition is falsy
	Syntax:
	(expression) ? ifTrue; : ifFalse;

*/

		

	let ternaryResult = (1>18) ? 1 : 2;
	console.log(ternaryResult);

// [SECTION] Switch statement

	/*
	The switch statement evaluates an expression and matches the expression's value to a case class

	*/
	let day = prompt("What day of week today?").toLowerCase();
	switch (day){
		case 'monday':
			console.log('The color of the day is red!');
			// code will stop
			break;
		case 'tuesday':
			console.log('The color of the day is orange!');
			break;
		case 'wednesday':
			console.log('The color of the day is yellow!');
			break;
		case 'thursday':
			console.log('The color of the day is green!');
			break;
		case 'friday':
			console.log('The color of the day is blue!');
			break;
		case 'saturday':
			console.log('The color of the day is indigo!');
			break;
		case 'sunday':
			console.log('The color of the day is violet!');
			break;
		default:
			console.log("Please input valid day.")
			break;
	}
//[SECTION] Try-catch-finally
	//try-catch
	// There are instances when the application returns an error/warning that is not necessarily an error in the context of our code
	// these errors are result of an attempt of the programming language to help developers in creating effecient code

function showIntensity(windSpeed) {
	// body...

	try{
		// codes that will be executed or run
		alert(determineTyphoonIntensity(windSpeed));
		alert("Intensity updates will show alert try.");
	}
	catch(error){
		console.warn(error.message);
	}
	finally{
		// Continue execution of code regardless of success and failure of code execution in the try statement
		alert("Intensity updates will show new alert from finally.")
	}
}
showIntensity(119);

		